Name:              pixman
Version:           0.44.2
Release:           1
Summary:           Pixman is a pixel manipulation library for X and Cairo
License:           MIT
URL:               https://gitlab.freedesktop.org/pixman/pixman
Source0:           https://www.x.org/releases/individual/lib/%{name}-%{version}.tar.xz

BuildRequires:     gcc make
BuildRequires:     meson >= 1.3.0

%description
Pixman is a low-level software library for pixel manipulation, providing features such\
\as image compositing and trapezoid rasterization.\

%package           devel
Summary:           Provide library and header files for pixman
Requires:          %{name} = %{version}-%{release}

%description       devel
Provide library and header files for pixman

%prep
%autosetup -n %{name}-%{version} -p1
sed -i 's/120/600/' test/meson.build

%build
%meson --auto-features=auto \
%ifarch %{arm}
  -Diwmmxt=disabled -Diwmmxt2=false \
%endif
  %nil

%meson_build

%install
%meson_install

%check
%meson_test

%files
%license COPYING
%{_libdir}/libpixman-1*.so.*

%files devel
%{_includedir}/pixman-1
%{_libdir}/libpixman-1*.so
%{_libdir}/pkgconfig/*

%changelog
* Tue Dec 03 2024 Funda Wang <fundawang@yeah.net> - 0.44.2-1
- update to 0.44.2

* Thu Nov 07 2024 Funda Wang <fundawang@yeah.net> - 0.44.0-1
- update to 0.44.0

* Tue Mar 05 2024 wangqia <wangqia@uniontech.com> - 0.43.4-1
- update to 0.43.4

* Fri Feb 03 2023 zhangpan <zhangpan@h-partners.com> - 0.42.2-1
- update to 0.42.2

* Tue Nov 08 2022 wangkerong <wangkerong@h-partners.com> - 0.40.0-3
- fix CVE-2022-44638

* Wed Oct 26 2022 zhouwenpei <zhouwenpei1@h-partners.com> - 0.40.0-2
- Rebuild for next release

* Mon Apr 20 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.40.0-1
- Type:enhancement
- Id:NA
- SUG:NA
- DESC:update to 0.40.0

* Mon Sep 9 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.38.0-1
- Type:enhancement
- Id:NA
- SUG:NA
- DESC:Modify the spec according to the new rules

* Thu Aug 22 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.34.0-11 
- Package init
